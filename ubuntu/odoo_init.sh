#!/usr/bin/env bash
# shellcheck disable=2016 disable=1091 disable=2059
version="2022-03-14"

DOMAIN_NAME='example.com'
EMAIL_ADDRESS='mail@example.com'

usage='USAGE : curl -LsS URL/odoo_init.sh | bash -s -- [OPTIONS]
Sample Options: 
--domain-name=example.com  
--email-address=mail@example.com
'
msg(){
    type=$1 #${1^^}
    shift
    printf "[$type] %s\n" "$@" >&2
}

error(){
    msg error "$@"
    exit 1
}

version(){
    printf "Odoo_cloud_init_setup %s\n" "$version"
}

while :; do
    case $1 in
		--version)
					version
					exit 0
					;;
		--domain-name)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						DOMAIN_NAME=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
        --domain-name=?*)
					DOMAIN_NAME=${1#*=}
					;;
		--email-address)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						EMAIL_ADDRESS=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--email-address=?*)
					EMAIL_ADDRESS=${1#*=}
					;;
		--help)
					version
					printf "%s" "$usage"
					exit
					;;
        -?*)
					msg warning "Unknown option (ignored): $1\n"
					;;
		*)
            break
    esac
    shift
done
		


wget -O - https://nightly.odoo.com/odoo.key | apt-key add -
echo "deb http://nightly.odoo.com/15.0/nightly/deb/ ./" >> /etc/apt/sources.list.d/odoo.list
apt -y update 
apt -y upgrade
apt -y install odoo
apt -y install postgresql
apt -y install nginx
apt -y install letsencrypt
apt -y install python3-certbot-nginx

#clear nginx default conf
echo "" > /etc/nginx/sites-enabled/default
cat << \EOF >> /etc/nginx/sites-enabled/default
# BEGIN Odoo Default Conf
server {
        listen 80 default_server;
        listen [::]:80 default_server;
        server_name _;

        location / {
                proxy_set_header Host $host;
                proxy_set_header X-Real-IP $remote_addr;
                proxy_pass http://127.0.0.1:8069;
        }
        location /longpolling {
                proxy_set_header Host $host;
                proxy_set_header X-Real-IP $remote_addr;
                proxy_pass http://127.0.0.1:8072;
         }
}
# END Odoo Default Conf
EOF

systemctl enable odoo
systemctl start odoo

#certbot --nginx -d $DOMAIN_NAME -m $EMAIL_ADDRESS --redirect --agree-tos -n
systemctl enable nginx
systemctl start nginx
